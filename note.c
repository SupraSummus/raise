#include "note.h"

#include <stdio.h>
#include <sys/mman.h>
#include <string.h>
#include "regnames.h"
#include <fcntl.h>
#include "weak_mmap.h"

static const bool DEBUG = false;

void process_note (Elf32_Nhdr * note, env_t * env) {
	if (DEBUG) {
		fprintf(stderr,
			"NOTE: namesz = %d, descsz = %d, type = %d, name = %s => ",
			note->n_namesz, note->n_descsz, note->n_type,
			(char *)note + sizeof(Elf32_Nhdr)
		);
	}
	
	void * data = pad4((char *)note + sizeof(Elf32_Nhdr) + note->n_namesz);
	
	switch (note->n_type) {
		
		// tls
		case NT_386_TLS:
			if (DEBUG) {
				fprintf(stderr, "NT_386_TLS\n");
			}
			
			if (env->tls_set) {
				fprintf(stderr, "tls juz bylo ustawine\n");
			}
			env->tls_set = true;
			
			// https://www.technovelty.org/linux/debugging-__thead-variables-from-coredumps.html
			
			env->tls_count = note->n_descsz / sizeof(struct user_desc);
			if (env->tls_count > tls_max_count) {
				env->tls_count = tls_max_count;
				fprintf(stderr, "zabraklo miejsca na wpisy tls\n");
			}
			
			if (DEBUG) {
				fprintf(stderr, "tls entries: %d\n", env->tls_count);
			}
			
			memcpy(env->tls_array, data, env->tls_count * sizeof(struct user_desc));
			
			break;
		
		// stan procesora
		case NT_PRSTATUS:
			if (DEBUG) {
				fprintf(stderr, "NT_PRSTATUS (prstatus structure)\n");
			}
			
			struct elf_prstatus * prstatus = data;
			
			if (env->gregs_set) {
				fprintf(stderr, "wielokrotne sekcje NT_PRSTATUS\n");
			}
			
			env->gregs_set = true;
			memcpy(env->gregs, prstatus->pr_reg, sizeof(env->gregs));
			
			if (DEBUG) {
				fprintf(stderr, "regs (%d):\n", ELF_NGREG);
				for (int i = 0; i < ELF_NGREG; i ++) {
					fprintf(stderr, "\t%d (%s): \t%#010x\n", i, USER_REG_NAMES[i], (unsigned int)env->gregs[i]);
				}
			}
			
			break;
		
		// mapped files
		case NT_FILE:
			if (DEBUG) {
				fprintf(stderr, "NT_FILE (mapped files)\n");
			}
			
			mapped_files_t * files_struct = data;
			mapped_file_t * files = files_struct->files;
			// nazwy zaczyanają się po zakonczeniu tablicy
			char * filenames = (char *)&files[files_struct->n_files];
			
			char * filename = filenames;
			for (
				int i = 0;
				
				i < files_struct->n_files;
				
				i ++,
				filename += strlen(filename) + 1
			) {
				mapped_file_t * file = files + i;
				
				if (DEBUG) {
					fprintf(stderr,
						"\tfile %d, name %s, start %p, end %p, off %d * %d\n",
						i, filename, file->start, file->end, file->offset, files_struct->page_size
					);
				}
				
				int file_fd = open(filename, O_RDONLY);
				if (file_fd == -1) {
					perror("nie udalo sie otworzyc pliku");
				}
				if (weak_mmap(2 /*| DONT_MMAP*/,
					file->start, (uint32_t)file->end - (uint32_t)file->start,
					PROT_NONE, MAP_PRIVATE | MAP_FIXED,
					file_fd, file->offset * files_struct->page_size
				) == MAP_FAILED) {
					perror("map failed");
				}
				close(file_fd);
			}
			
			break;
		
		// nieznany typ note
		default:
			if (DEBUG) {
				fprintf(stderr, "unknown note\n");
			}
			break;
	}
}
