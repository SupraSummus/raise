CC = gcc
CFLAGS = -Wall --pedantic -std=gnu99 -g
LDFLAGS = -static -Wl,-Ttext-segment=01000000

all: raise

raise: main.o enter.o note.o program_header.o weak_mmap.o
	$(CC) $(LDFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $^

%.o: %.S
	$(CC) $(CFLAGS) -c -o $@ $^

z1_test/raise: raise
	cp $^ $@

test: z1_test/raise
	make -C z1_test/ all

clean:
	rm -rf raise z1_test/raise *.o
	make -C z1_test/ clean
