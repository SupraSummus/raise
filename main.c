#include <fcntl.h>
#include <signal.h> // stack_t
#include <stdio.h> //printf scanf
#include <stdlib.h> // malloc
#include <sys/mman.h> // mmap
#include <sys/syscall.h> // syscall, SYS_set_thread_area
#include <unistd.h>
#include <ucontext.h> // set/getcontext
#include <stdbool.h>
#include "enter.h"
#include "utils.h"
#include "note.h"
#include "program_header.h"
#include "weak_mmap.h"

static const bool DEBUG = false;

static void * const stack_ptr = (void*)0x06000000;

static void check_ehdr (Elf32_Ehdr * ehdr) {
	if (
		ehdr->e_ident[EI_MAG0] != ELFMAG0 ||
		ehdr->e_ident[EI_MAG1] != ELFMAG1 ||
		ehdr->e_ident[EI_MAG2] != ELFMAG2 ||
		ehdr->e_ident[EI_MAG3] != ELFMAG3
	) {
		fprintf(stderr, "bad elf file (magic header)\n");
		exit(EXIT_FAILURE);
	}
	
	if (
		ehdr->e_ident[EI_CLASS] != ELFCLASS32 ||
		ehdr->e_ident[EI_DATA] != ELFDATA2LSB ||
		ehdr->e_ident[EI_VERSION] != EV_CURRENT ||
		//ehdr->e_ident[EI_OSABI] != ELFOSABI_LINUX ||
		ehdr->e_ident[EI_OSABI] != ELFOSABI_SYSV ||
		ehdr->e_ident[EI_ABIVERSION] != 0 ||
		ehdr->e_machine != EM_386 ||
		ehdr->e_version != EV_CURRENT
	) {
		fprintf(stderr, "bad elf file (architecture specification)\n");
		exit(EXIT_FAILURE);
	}
	
	if (
		ehdr->e_type != ET_CORE
	) {
		fprintf(stderr, "bad elf file (not a core file)\n");
		exit(EXIT_FAILURE);
	}
}

static void load_core (int fd) {
	if (DEBUG) {
		fprintf(stderr, "stack load_core(%d) = %p\n", fd, (void *)&fd);
	}
	
	// header
	Elf32_Ehdr ehdr;
	pread_all(fd, &ehdr, sizeof(ehdr), 0);
	
	check_ehdr(&ehdr);
	
	// program headers
	Elf32_Phdr * phdrs = malloc(sizeof(Elf32_Phdr) * ehdr.e_phnum);
	if (phdrs == NULL) {
		perror("nie udalo sie zaalokowac pamieci");
		exit(EXIT_FAILURE);
	}
	pread_all(fd, phdrs, sizeof(Elf32_Phdr) * ehdr.e_phnum, ehdr.e_phoff);
	
	env_t env;
	env.gregs_set = false;
	env.fd = fd;
	env.tls_set = false;
	
	for (int i = 0; i < ehdr.e_phnum; i ++) {
		process_program_header(phdrs + i, &env);
	}
	
	// free
	free(phdrs);
	close(fd);
	env.fd = -1;
	
	if (!env.gregs_set) {
		fprintf(stderr, "process regs info not present\n");
		exit(EXIT_FAILURE);
	}
	
	if (DEBUG) {
		// poczekaj
		fprintf(stderr, "czytanie zakonczone\n");
		getchar();
	}
	
	if (env.tls_set) {
		if (DEBUG) {
			fprintf(stderr, "seting tls\n");
		}
		for (int i = 0; i < env.tls_count; i ++) {
			if (syscall(SYS_set_thread_area, &(env.tls_array[i])) != 0) {
				perror("set_thred_area() sie nie udalo");
				exit(EXIT_FAILURE);
			}
		}
	}
	
	enter((struct user_regs_struct *)env.gregs);
	
	fprintf(stderr, "enter() powrocilo, zle\n");
	exit(EXIT_FAILURE);
}

int main (int argc, char ** argv) {
	if (DEBUG) {
		int stack;
		fprintf(stderr, "stack main() = %p, pid = %d\n", (void *)&stack, getpid());
		(void)stack;
		getchar();
	}
	
	if (argc != 2) {
		fprintf(stderr, "usage: %s core_file\n", argv[0]);
		exit(EXIT_FAILURE); // a moze sukces?
	}
	
	char * filename = argv[1];
	int fd = open(filename, O_RDONLY);
	if (fd == -1) {
		perror("nie udalo sie otworzyc pliku core");
		exit(EXIT_FAILURE);
	}
	
	ucontext_t context;
	if (getcontext(&context) != 0) {
		perror("getcontext() sie nie udal");
		exit(EXIT_FAILURE);
	}
	context.uc_link = NULL; // program ma sie zakonczyc gdy nowy kontekst wyjdzie
	
	// alloc and move stack
	size_t stack_size = getpagesize();// jedna strona ale moze rosnąć
	void* new_stack = mmap(
		(char *)stack_ptr - stack_size, stack_size,
		PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED | MAP_GROWSDOWN,
		-1, 0 // bez pliku
	);
	
	if (new_stack == MAP_FAILED) {
		perror("nie udalo sie zaalokowac stosu");
		exit(EXIT_FAILURE);
	}
	
	if (DEBUG) {
		fprintf(stderr, "move stack to %p\n", stack_ptr);
	}
	context.uc_stack.ss_sp = stack_ptr;
	context.uc_stack.ss_size = 0;
	
	// ustawienie "entry pointa" dla nowego stosu
	makecontext(&context, (void (*)())load_core, 1, fd);
	
	// zabezpieczenie przed modyfikacją adresów uzywanych przez raise
	// (adresy ponizej adresu nowego stosu)
	if (weak_mmap(0 | DONT_MMAP, NULL, (size_t)stack_ptr, 0, 0, -1, 0) == MAP_FAILED) {
		perror("weak_mmap - protect all");
	}
	
	// set stack - to zaczyna wykonywać load_core()
	if (setcontext(&context) != 0) {
		perror("nie udalo sie zmienic adresu stosu");
		exit(EXIT_FAILURE);
	}
	
	// nie powinniśmy tu wrócić
	fprintf(stderr, "setcontext() powrocilo, źle\n");
	exit(EXIT_FAILURE);
}
