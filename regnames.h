#ifndef REGNAMES_H
#define REGNAMES_H

// dla strukturki elf_gregset_t: indeks -> nazwa rejestru
static const char const * USER_REG_NAMES[] = {
  "ebx",
  "ecx",
  "edx",
  "esi",
  "edi",
  "ebp",
  "eax",
  "xds",
  "xes",
  "xfs",
  "xgs",
  "orig_eax",
  "eip",
  "xcs",
  "eflags",
  "esp",
  "xss",
};

#endif
