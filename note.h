#ifndef NOTE_H
#define NOTE_H

#include "utils.h"
#include <elf.h>

extern void process_note (Elf32_Nhdr * note, env_t * env);

#endif
