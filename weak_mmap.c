#include "weak_mmap.h"
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

static const bool DEBUG = false;

typedef struct _block_t block_t;
struct _block_t {
	block_t * next;
	void * addr;
	size_t size;
	int prio;
};

static block_t * head = NULL;

static block_t * insert_at(block_t ** link) {
	block_t * new = malloc(sizeof(block_t));
	if (new == NULL) {
		return NULL;
	}
	new->next = *link;
	*link = new;
	return new;
}

void * weak_mmap(unsigned int prio, void * addr, size_t length, int prot, int flags, int fd, off_t offset) {
	// inicjalizacja strażnika
	if (head == NULL) {
		if (DEBUG) {
			fprintf(stderr, "weak_mmap(): init\n");
		}
		head = insert_at(&head);
		if (head == NULL) {
			return MAP_FAILED;
		}
		head->addr = (void *)-1;
		head->size = 0;
		head->prio = 0;
	}
	
	bool do_mmap = !(prio & DONT_MMAP);
	prio = prio & ~DONT_MMAP;
	size_t mapped = 0; // ile juz zmapowalismy (albo pominelismy)
	prot |= MAP_FIXED;
	
	for (block_t * i = head; i != NULL; i = i->next) {
		
		// mapujemy to co zmiesciło sie miedzy poprzednim i aktualnym ogniwem listy
		// (w zasadzie mapujemy wszystko czego nie zmapowalismy i miesci sie przed aktualnym ogniwem listy)
		//                                     nie mapujemy jesli juz wszystko zmapowalismy
		if ((size_t)addr + mapped < (size_t)i->addr && 0 < length - mapped) {
			// d = długosc tego mapowania = min(tyle co sie zmiesci, length)
			// d nie jest zerem
			size_t d = (size_t)i->addr - ((size_t)addr + mapped);
			if (d > (length - mapped)) {
				d = length - mapped;
			}
			
			if (DEBUG) {
				fprintf(stderr, "weak_mmap(): new mmap \t%p, \t%zu\n", (char *)addr + mapped, d);
			}
			
			if (do_mmap) {
				if (mmap(
					(char *)addr + mapped, d,
					prot, flags, fd, offset + mapped
				) == MAP_FAILED) {
					return MAP_FAILED;
				}
			}
			
			// dołożenie do listy
			block_t * new = insert_at(&i->next);
			if (new == NULL) {
				return MAP_FAILED;
			}
			new->addr = i->addr;
			new->size = i->size;
			new->prio = i->prio;
			i->addr = (char *)addr + mapped;
			i->size = d;
			i->prio = prio;
			
			mapped += d;
		}
		
		// przetwarzanie tego co nachodzi na aktualne ogniwo listy
		if ((size_t)addr + mapped < (size_t)i->addr + i->size && 0 < length - mapped) {
			// d = dlugosc nałożenia
			size_t d = (size_t)i->addr + i->size - ((size_t)addr + mapped);
			if (d > (length - mapped)) {
				d = length - mapped;
			}
			
			if (prio > i->prio) {
				// pomijamy
				if (DEBUG) {
					fprintf(stderr, "weak_mmap(): skip at \t%p, \t%zu\n", (char *)addr + mapped, d);
				}
			} else {
				// trzeba nadpisać
				if (DEBUG) {
					fprintf(stderr, "weak_mmap(): overwrite \t%p, \t%zu\n", (char *)addr + mapped, d);
				}
				
				if (do_mmap) {
					if (mmap(
						(char *)addr + mapped, d,
						prot, flags, fd, offset + mapped
					) == MAP_FAILED) {
						return MAP_FAILED;
					}
				}
				
				// aktualizacja listy
				block_t * before, * new, * after;
				void * baddr = i->addr;
				size_t blen = i->size;
				int bprio = i->prio;
				
				if ((size_t)addr + mapped == (size_t)baddr && d == blen) {
					// podmieniamy caly
					before = NULL;
					new = i;
					after = NULL;
				} else if ((size_t)addr + mapped == (size_t)baddr) {
					// początek ten sam
					before = NULL;
					new = i;
					after = insert_at(&i->next);
					if (after == NULL) {
						return MAP_FAILED;
					}
				} else if ((size_t)addr + mapped + d == (size_t)baddr + blen) {
					// koniec ten sam
					before = i;
					new = insert_at(&i->next);
					after = NULL;
					if (new == NULL) {
						return MAP_FAILED;
					}
				} else {
					// nowy blok jest w srodku starego
					before = i;
					new = insert_at(&i->next);
					if (new == NULL) {
						return MAP_FAILED;
					}
					after = insert_at(&new->next);
					if (after == NULL) {
						return MAP_FAILED;
					}
				}
				
				if (before != NULL) {
					before->addr = baddr;
					before->size = (size_t)addr + mapped - (size_t)baddr;
					before->prio = bprio;
				}
				
				if (new != NULL) {
					new->addr = addr;
					new->size = d;
					new->prio = prio;
				}
				
				if (after != NULL) {
					after->addr = (char *)addr + mapped + d;
					after->size = (size_t)baddr + blen - ((size_t)addr + mapped + d);
					after->prio = bprio;
				}
			}
			
			mapped += d;
		}
		
	}
	
	return addr;
}
