#ifndef PROGRAM_HEADER_H
#define PROGRAM_HEADER_H

#include "utils.h"
#include <elf.h>

extern void process_program_header (Elf32_Phdr * phdr, env_t * env);

#endif
