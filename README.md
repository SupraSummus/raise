# Kompilacja i użycie

    make
    ./raise <core file>

# Opis rozwiązania

Program jest zlinkowany statycznie, poniżej standardowego adresu ładowania (`Makefile`). W czasie działania najpierw przenosi swój stos w niskie obszary pamięci (`main()`), potem czyta podany plik core i na jego podstawie mapuje pliki do pamięci (`load_core()`, `pocess_program_header()`, `process_note()`, `weak_mmap()`), ustawia TLS (`load_core()`) , a następnie za pomocą krótkiej funkcji napisanej w asemblerze (`enter.S`) ładuje zapisane wartosci rejestrów i skacze do odpowiedniej instrukcji.

W funkcji `weak_mmap()` obsługiwane jest "nadpisywanie" jednych segmentów opisanych w pliku core innymi. Jest to realizowane poprzez wzbogacenie standardowego `mmap`a priorytetami. Algorytmicznie nie jest to nic wyszukanego - lista wskaźnikowa segmentów pamięci. Cała oparacja odtwarzania pamięci ma w sumie złożoność kwadratową względem liczby segmentów. Alternatywą do samodzielnego napisania struktur używanych w `weak_mmap` było użycie
 * `posix_mem_offset` (funkcji, której nie ma na wirtualkach zso)
 * `msync` + sprawdzanie `ENOMEM` (trzeba to wykonać dla każdej strony osobno)
 * `mincore` - o ile dobrze zrozumiałem to by nie zadziałało w przypadku swapowania pamięci

Dodatkowym wzbogaceniem względem treści zadania (o ile dobrze zrozumiałem wymagania) jest oznaczanie stosu jako segmentu rozszerzalnego w dół. Segment stosu jest wykrywany na podstawie wartości `esp`. O ile poniżej tego segmnetu jest niezmapowane miejsce, dokładane jest tam mapowanie o rozmiarze jednej strony z flagą `MAP_GROWSDOWN`.

# Zadanie

Wskrzesić zmarły proces - napisać program ładujący plik core do pamięci i tworzący na jego podstawie nowy, żywy proces.

# Zakres odtwarzania procesu

Pliki core zawierają dane procesu - można więc użyć tych danych do odtworzenia procesu i przywrócenia go do życia. Niestety, nie wszystko da się odtworzyć:

 * pliki core nie zawierają tabeli otwartych plików - zakładamy więc, że program nie miał otwartych żadnych plików poza standardowym wejściem/wyjściem/wyjściem błędów (które należy podłączyć do standardowego wejścia/wyjścia/wyjścia błędów procesu odtwarzającego).
 * ciężko zapewnić, że nowy proces będzie miał taki sam pid, jak oryginał - nie przejmujemy się tym i mamy nadzieję, że nic złego się nie stanie.
 * zakładamy, że proces nie używa wątków
 * procesy mają masę różnego stanu związanego np. z układem pamięci (patrz man 2 prctl) - nie da się tego odtworzyć na podstawie core, więc również mamy nadzieję, że nie będzie potrzebne.

Rozwiązanie powinno odtwarzać:

 * zmapowane pliki (ze struktury NT_FILE)
 * dane w zmapowanych obszarach (z segmentów PT_LOAD)
 * rejestry ogólnego przeznaczenia (eax, ecx, edx, ebx, esp, ebp, esi, edi) (ze struktury NT_PRSTATUS)
 * rejestry eip, eflags (z tej samej struktury)
 * adres segmentu TLS przez set_thread_area (z NT_386_TLS)

Zakładamy również, że proces nie umarł na “naturalny” SIGSEGV, tylko np. na SIGQUIT z klawiatury - w przypadku błędu wykonania, wskrzeszenie procesu spowoduje natychmiastowe powtórzenie błędnej instrukcji i zakończenie procesu.

Rozwiązanie powinno działać na procesach 32-bitowych na procesorach x86.

Można założyć, że proces miał standardowy układ pamięci - np. dozwolone jest zajęcie na swoje potrzeby obszaru poniżej standardowego adresu ładowania programu (0x8048000).

# Forma rozwiązania

Rozwiązanie może być napisane w dowolnym języku, którego interpreter/kompilator znajduje się w repozytorium paczek sensownej dystrybucji (w tym w mieszance języków). Rozwiązanie może używać tylko standardowej biblioteki dla odpowiednich języków programowania.

Jako rozwiązanie należy dostarczyć paczkę zawierającą:

 * dowolną liczbę plików źródłowych z kodem rozwiązania
 * plik `Makefile` kompilujący rozwiązanie, lub odpowiadający plik z innego sensownego systemu budowania (np. `cmake`)
 * plik `README` zawierający krótki opis rozwiązania i instrukcję kompilacji

Po skompilowaniu rozwiązania, polecenie:

    ./raise <plik core>

powinno stworzyć nowy proces na podstawie danego pliku core, i zakończyć się, gdy ten proces zakończy pracę (z tym samym kodem wyjścia, jeśli wyjście nastąpiło przez `exit`).

