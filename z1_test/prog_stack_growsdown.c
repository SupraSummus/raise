#include <unistd.h>
#include <signal.h>

void recursion (int n) {
    char table[128];
    write(1, "a", 1);
    if (n > 0) {
        recursion(n - 1);
    }
    write(1, "A", 1);
}

int main() {
    raise(SIGQUIT);
    recursion(10000);
    return 42;
}
