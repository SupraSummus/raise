#ifndef UTILS_H
#define UTILS_H

#include <asm/ldt.h> // struct user_desc
#include <stdint.h>
#include <stdbool.h>
#include <sys/procfs.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
	void * start;
	void * end;
	uint32_t offset;
} mapped_file_t;

typedef struct {
	// http://www.gabriel.urdhr.fr/2015/05/29/core-file/
	uint32_t n_files;
	uint32_t page_size;
	mapped_file_t files[];
} mapped_files_t;

typedef struct {
	/* core file fd */
	int fd;
	
	/* rejestry */
	bool gregs_set;
	elf_gregset_t gregs;
	
	/* tls */
	bool tls_set;
	size_t tls_count;
	#define tls_max_count (16)
	struct user_desc tls_array[tls_max_count];
} env_t;

inline static uint32_t pad4int(uint32_t n) {
	return (n + 3) / 4 * 4;
}

inline static void * pad4 (void * ptr) {
	uint32_t intptr = (int32_t)ptr;
	return (void *)(pad4int(intptr));
}

inline static void pread_all(int fd, void * buf, size_t n, off_t off) {
	while (n > 0) {
		ssize_t res = pread(fd, buf, n, off);
		if (res <= 0) {
			perror("pread error");
			exit(EXIT_FAILURE);
		}
		buf = (char *)buf + res;
		off += res;
		n -= res;
	}
}

#endif
