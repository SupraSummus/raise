#include "program_header.h"
#include <stdio.h>
#include <sys/mman.h>
#include <stdlib.h>
#include "note.h"
#include "weak_mmap.h"

static const bool DEBUG = false;

void process_program_header (Elf32_Phdr * phdr, env_t * env) {
	switch (phdr->p_type) {
		// ładowanie kawałków z pliku core
		case PT_LOAD:
			if (DEBUG) {
				fprintf(stderr,
					"PT_LOAD offset = %p, vaddr = %p, filesz = %d, memsz = %d, flags = %c%c%c\n",
					(void *)phdr->p_offset, (void *)phdr->p_vaddr, phdr->p_filesz, phdr->p_memsz,
					phdr->p_flags & PF_R ? 'R' : '-', phdr->p_flags & PF_W ? 'W' : '-', phdr->p_flags & PF_X ? 'X' : '-'
				);
			}
			
			// w specyfikacji jest ze filesz moze byc wieksze niz memsz,
			// ale taka sytuacja nie wnosi nic nowego, wiec ją usuwamy
			if (phdr->p_filesz > phdr->p_memsz) {
				phdr->p_filesz = phdr->p_memsz;
			}
			
			// podmapowanie cora w przypadku gdy strony nie są wyrównane
			//if (phdr->p_filesz != 0) {
				/*
				// alokacja stron
				if (mmap(
					(void *)phdr->p_vaddr, phdr->p_memsz,
					PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS | maybe_growsdown,
					-1, 0
				) == MAP_FAILED) {
					perror("failed to map");
				}
				
				// kopiuj plik
				pread(env->fd, (void *)phdr->p_vaddr, phdr->p_filesz, phdr->p_offset); // TODO min(filesz, memsz)
				*/
			//}
			
			// podmapowanie cora
			if (weak_mmap(1 /*| DONT_MMAP*/,
				(void *)phdr->p_vaddr, phdr->p_filesz,
				PROT_NONE, MAP_PRIVATE | MAP_FIXED,
				env->fd, phdr->p_offset
			) == MAP_FAILED) {
				perror("failed to map");
			}
			
			// mapowanie pamieci anonimowej (jeśli to nie są kawałki podmapowane w NOTE)
			if (weak_mmap(3 /*| DONT_MMAP*/,
				(void *)phdr->p_vaddr, phdr->p_memsz,
				PROT_NONE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,
				-1, 0
			) == MAP_FAILED) {
				perror("failed to map");
			}
			
			// uprawnienia
			int prot =
				(phdr->p_flags & PF_R ? PROT_READ : 0) |
				(phdr->p_flags & PF_W ? PROT_WRITE : 0) |
				(phdr->p_flags & PF_X ? PROT_EXEC : 0);
			if (mprotect((void *)phdr->p_vaddr, phdr->p_memsz, prot) != 0) {
				perror("nie udal sie mprotect");
			}
			
			// czy esp wskazuje na coś  w srdoku tego segmentu?
			// jeśli tak to robimy stos
			if (env->gregs_set) {
				unsigned int esp = ((struct user_regs_struct *)&env->gregs)->esp;
				if (phdr->p_vaddr <= esp && esp <= phdr->p_vaddr + phdr->p_memsz) {
					if (DEBUG) {
						fprintf(stderr, "jest stack segment!\n");
					}
					
					size_t page_size = getpagesize();
					
					if (weak_mmap(4,
						(void *)(phdr->p_vaddr - page_size), page_size,
						PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS | MAP_GROWSDOWN,
						-1, 0
					) == MAP_FAILED) {
						perror("failed to map");
					}
					
				}
			}
			
			break;
		
		// stan procesora + podmapowane pliki + ...
		case PT_NOTE:
			if (DEBUG) {
				fprintf(stderr, "PT_NOTE\n");
			}
			
			void * notes = malloc(phdr->p_filesz);
			if (notes == NULL) {
				perror("nie udalo sie zaalokowac pamieci");
			}
			pread_all(env->fd, notes, phdr->p_filesz, phdr->p_offset);
			
			// iteracja po headerach note
			for (
				Elf32_Nhdr * note = notes;
				
				// end of structure                   end of notes segment
				(char *)note + sizeof(Elf32_Nhdr) <= (char *)notes + phdr->p_filesz;
				
				// nastepny note header
				note = (void *)((char *)note + sizeof(Elf32_Nhdr) + pad4int(note->n_namesz) + pad4int(note->n_descsz))
			) {
				process_note(note, env);
			}
			
			free(notes);
			break;
		
		// tego nie obslugujemy, phi
		default:
			if (DEBUG) {
				fprintf(stderr, "unknown program header\n");
			}
			break;
	}
}
