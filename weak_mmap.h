#ifndef WEAK_MMAP_H
#define WEAK_MMAP_H

#include <stddef.h>
#include <sys/types.h>

// dziala jak mmap z priorytetami
// nie rzuca błedów dla length = 0
// ma sens tylko z MAP_FIXED
// prio to priorytet - mniejsza wartosc nadpisuje strony zmapowane z wiekszą wartoscią
// maska DONT_MMAP zawarta w priorytecie powoduje ze mmap nie zostanie wykonany i jedynie miejsce bedzie oznaczone jako zajete
extern void * weak_mmap(unsigned int prio, void * addr, size_t length, int prot, int flags, int fd, off_t offset);

static const unsigned int DONT_MMAP = 1 << 31;

#endif
